package com.eStore.persistence.dao;

import java.util.ArrayList;
import java.util.Random;

import com.eStore.entities.Customer;
import com.eStore.entities.Order;
import com.eStore.entities.Product;

public class DummyData {

	Random rnd = new Random();
	ArrayList<Order> orderHistory = new ArrayList<Order>();
	ArrayList<Product> cartList = new ArrayList<Product>();
	ArrayList<Product> products = new ArrayList<Product>();

	public DummyData() {
		
		products.add(new Product(0, "I911900KF", 499.99));
		products.add(new Product(1, "I711700KF", 399.99));
		products.add(new Product(2, "R95950X", 599.99));
		products.add(new Product(3, "R75800X", 399.99));
		products.add(new Product(4, "R55600X", 299.99));
		
	}
	
	
	
	public Order createOrder(int customerId) {

		ArrayList<Customer> customers = getCustomers();
		Order order;
		double sum = 0.00;

		for (Product product : cartList) {
			sum = product.getPrice() + sum;
		}

		order = new Order(orderHistory.size(), customers.get(customerId), cartList, sum);
		orderHistory.add(order);
		System.out.println("Done");
		return order;

	}

	public void setCart(int id) {

		Product product = null;

		for (int i = 0; i <= products.size() - 1; i++) {

			if (id == products.get(i).getId())
				product = products.get(i);
			else
				product = new Product(products.size() + 1, product.getName(), 999.99);
		}

		
		cartList.add(product);

	}
	

	public ArrayList<Order> getOrderHistory() {

		return orderHistory;
	}


	public ArrayList<Customer> getCustomers() {

		ArrayList<Customer> customerList = new ArrayList<Customer>();
		customerList.add(new Customer(0, "AndersBoss", "anders@ya.se"));
		customerList.add(new Customer(1, "Daniel", "daniel@ya.se"));
		customerList.add(new Customer(2, "Daisy", "daisy@home.se"));
		customerList.add(new Customer(3, "Medi", "madison@home.se"));
		customerList.add(new Customer(4, "mvpUser", "mvpuser@azure.se"));

		return customerList;

	}



	public ArrayList<Product> getCartList() {
		return cartList;
	}



	public void setCartList(ArrayList<Product> cartList) {
		this.cartList = cartList;
	}



	public ArrayList<Product> getProducts() {
		return products;
	}



	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}



	public void setOrderHistory(ArrayList<Order> orderHistory) {
		this.orderHistory = orderHistory;
	}

	
	
}
