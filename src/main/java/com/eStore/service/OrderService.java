package com.eStore.service;

import java.util.ArrayList;

import javax.enterprise.context.ApplicationScoped;

import com.eStore.entities.Customer;
import com.eStore.entities.Order;
import com.eStore.entities.Product;
import com.eStore.persistence.dao.DummyData;


@ApplicationScoped
public class OrderService {
	
	private DummyData dummyData = new DummyData();
	
	public ArrayList<Order> getHistory() {
		
		return dummyData.getOrderHistory();
		
	}
	
	public Order createOrder(int customerId) {
		
		return dummyData.createOrder(customerId);
	}
	
	public void setCart(int id) {
		
		dummyData.setCart(id);
		
	}
	
	public ArrayList<Product> getCart() {

		return dummyData.getCartList();

	}

}
