package com.eStore.service;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import com.eStore.entities.Product;
import com.eStore.persistence.dao.DummyData;

@ApplicationScoped
public class ProductService {

		
	private DummyData dummyData = new DummyData();
	
	public ArrayList<Product> getProducts() {
		
		return dummyData.getProducts();
		
	}
	
}
