package com.eStore.service;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import com.eStore.entities.Customer;
import com.eStore.persistence.dao.DummyData;


@ApplicationScoped
public class CustomerService {

	
	private DummyData dummyData = new DummyData();
	
	public ArrayList<Customer> getCustomers() {
		
		return dummyData.getCustomers();
		
	}
	
}
