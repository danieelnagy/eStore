package com.eStore.boundary;

import java.util.ArrayList;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.eStore.entities.Customer;
import com.eStore.entities.Order;
import com.eStore.entities.Product;
import com.eStore.service.OrderService;

@Path("orders")
public class OrderREST {

	@Context
	UriInfo uriInfo;
	@Inject
	private OrderService orderService;

	@POST
	@Path("createOrder/{customerId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Order create(@PathParam("customerId") int customerId) {

		return orderService.createOrder(customerId);

	}

	@POST
	@Path("addToCart/{productId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addToCart(@PathParam("productId") int id) {

		
		orderService.setCart(id);
		return Response.ok(id).build();

	}
	
	
	@GET
	@Path("getCart")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Product> getCart() {
		return orderService.getCart();
	}

	@GET
	@Path("getOrders")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrders() {

		ArrayList<Order> history = orderService.getHistory();
		if (history.isEmpty())
			return Response.noContent().build();

		return Response.ok(history).build();
	}

}
