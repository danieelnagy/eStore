package com.eStore.boundary;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.eStore.entities.Customer;
import com.eStore.service.CustomerService;

@Path("customers")
public class CustomerREST {

	@Context
	UriInfo uriInfo;

	@Inject
	private CustomerService customerService;

	@GET
	@Path("getCustomers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomers() {

		ArrayList<Customer> customers = customerService.getCustomers();

		if (customers.isEmpty())
			return Response.noContent().build();

		return Response.ok(customers).build();

	}
}
