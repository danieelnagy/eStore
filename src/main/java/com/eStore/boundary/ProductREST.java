package com.eStore.boundary;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.eStore.entities.Product;
import com.eStore.service.ProductService;

@Path("products")
public class ProductREST {

	@Context
	UriInfo uriInfo;

	@Inject
	private ProductService productService;

	@GET
	@Path("getProducts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProducts() {

		ArrayList<Product> products = productService.getProducts();

		if (products.isEmpty())
			return Response.noContent().build();

		return Response.ok(products).build();
	}

}
