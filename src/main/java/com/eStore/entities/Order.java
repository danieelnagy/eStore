package com.eStore.entities;

import java.io.Serializable;
import java.util.List;

public class Order implements Serializable {


	private static final long serialVersionUID = 1L;
	private int id;
	private Customer customer;
	private List<Product> product;
	private double sum;
	
	public Order(int id, Customer customer, List<Product> product, double sum) {
		super();
		this.id = id;
		this.customer = customer;
		this.product = product;
		this.sum = sum;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", customer=" + customer + ", product=" + product + ", sum=" + sum + "]";
	}

	

}
