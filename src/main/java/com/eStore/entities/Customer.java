package com.eStore.entities;

import java.io.Serializable;

public class Customer implements Serializable{


	private static final long serialVersionUID = 1L;
	private int id;
	private String fullName;
	private String emailAddress;

	public Customer(int id, String fullName, String emailAddress) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.emailAddress = emailAddress;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", fullName=" + fullName + ", emailAddress=" + emailAddress + "]";
	}

}
